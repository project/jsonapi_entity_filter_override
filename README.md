### Introduction
This module allows altering the resolved filter of the JSON:API endpoints.

### How does this work?
This module alters the `jsonapi.entity_resource` service's controller class
with a new one which extends it. This allows the creation of the
`hook_json_entity_filter_override_alter()` which allows modules to change the
resolved filter of the JSON:API endpoints.

### How is this useful?
According to [#3034701](https://www.drupal.org/project/jsonapi/issues/3034701)
and an example provided in the [documentation](https://www.drupal.org/docs/8/modules/jsonapi/fetching-resources-get#s-get-user-accounts)
there is no way to filter entities based on a field referencing a configuration
entity (at least until [#3036593](https://www.drupal.org/project/drupal/issues/3036593)
is resolved).

This means that you are not allowed to use `filter[roles.id]=content_manager`
to retrieve all the users of the `content_manager` role since `roles.id` gets
translated to `roles.entity:user_role.uuid` and Drupal's Entity API doesn't
allow querying configuration entities (eg. `user_role.uuid`). The truth is
though that we don't need to query configuration entities. We could just
replace `.entity:user_role.uuid` with `.target_id` in the translation.

That's what this module does out of the box.

### Configuration
No configuration is needed.

If you need to support other configuration entities than the `user_roles`, feel
free to use the hook provided.

### Why is this module not named *JSON:API Configuration Entity Filter Override*?
This was my original intention. But the override required for configuration
entities also works for content entities.
