<?php

/**
 * @file
 * Documentation related to "JSON:API - Configuration Entities Filter".
 */

/**
 * Allows altering the filter details from a URL.
 *
 * @param array $filter_item
 *   The details of the filter_item.
 */
function hook_json_entity_filter_override_alter(array &$filter_item) {
  // Modify the <code>$filter_item['condition']['path']</endcode>.
}
