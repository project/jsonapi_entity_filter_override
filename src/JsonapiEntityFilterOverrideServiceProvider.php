<?php

namespace Drupal\jsonapi_entity_filter_override;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the jsonapi.entity_resource service.
 */
class JsonapiEntityFilterOverrideServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('jsonapi.entity_resource');
    $definition->setClass('Drupal\jsonapi_entity_filter_override\Controller\JsonApiFilterOverrideEntityResource');
  }

}
