<?php

namespace Drupal\jsonapi_entity_filter_override\Controller;

use Drupal\jsonapi\Controller\EntityResource;
use Drupal\jsonapi\Query\Filter;
use Drupal\jsonapi\Query\OffsetPage;
use Drupal\jsonapi\Query\Sort;
use Drupal\jsonapi\ResourceType\ResourceType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class JsonApiFilterOverrideEntityResource.
 *
 * @package Drupal\dave\Controller
 */
class JsonApiFilterOverrideEntityResource extends EntityResource {

  /**
   * Extracts JSON:API query parameters from the request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param \Drupal\jsonapi\ResourceType\ResourceType $resource_type
   *   The JSON:API resource type.
   *
   * @return array
   *   An array of JSON:API parameters like `sort` and `filter`.
   */
  protected function getJsonApiParams(Request $request, ResourceType $resource_type) {
    if ($request->query->has('filter')) {
      $params[Filter::KEY_NAME] = JsonApiFilterOverrideEntityResourceFilter::createFromQueryParameter(
        $request->query->get('filter'),
        $resource_type, $this->fieldResolver
      );
    }
    if ($request->query->has('sort')) {
      $params[Sort::KEY_NAME] = Sort::createFromQueryParameter(
        $request->query->get('sort')
      );
    }
    if ($request->query->has('page')) {
      $params[OffsetPage::KEY_NAME] = OffsetPage::createFromQueryParameter(
        $request->query->get('page')
      );
    }
    else {
      $params[OffsetPage::KEY_NAME] = OffsetPage::createFromQueryParameter([
        'page' => [
          'offset' => OffsetPage::DEFAULT_OFFSET,
          'limit' => OffsetPage::SIZE_MAX,
        ],
      ]);
    }
    return $params;
  }

}
