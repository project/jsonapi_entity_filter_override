<?php

namespace Drupal\jsonapi_entity_filter_override\Controller;

use Drupal\jsonapi\Context\FieldResolver;
use Drupal\jsonapi\Query\EntityCondition;
use Drupal\jsonapi\Query\Filter;
use Drupal\jsonapi\ResourceType\ResourceType;

/**
 * Class JsonApiFilterOverrideEntityResourceFilter.
 *
 * @package Drupal\jsonapi_entity_filter_override\Controller
 */
class JsonApiFilterOverrideEntityResourceFilter extends Filter {

  /**
   * {@inheritdoc}
   */
  public static function createFromQueryParameter($parameter, ResourceType $resource_type, FieldResolver $field_resolver) {
    $module_handler = \Drupal::moduleHandler();
    $expanded = static::expand($parameter);
    foreach ($expanded as &$filter_item) {
      if (isset($filter_item[static::CONDITION_KEY][EntityCondition::PATH_KEY])) {
        $unresolved = $filter_item[static::CONDITION_KEY][EntityCondition::PATH_KEY];
        $filter_item[static::CONDITION_KEY][EntityCondition::PATH_KEY] = $field_resolver->resolveInternalEntityQueryPath($resource_type, $unresolved);
        $module_handler->alter('json_entity_filter_override', $filter_item);
      }
    }
    return new static(static::buildEntityConditionGroup($expanded));
  }

}
